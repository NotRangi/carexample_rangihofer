package at.hof.car;

import java.time.LocalDate;
import java.time.Month;

public class Test {
	
	public static void main(String[] args) {
		
		Engine e1 = new Engine("Benzin", 150);
		Engine e2 = new Engine("Diesel", 100);
		
		Producer p1 = new Producer("Fritz", "Austria", 10);
		Producer p2 = new Producer("Susan", "UK", 8);
		
		Car c1 = new Car("red", 90, 23000, 15.2, 60000, p1, e1);
		Car c2 = new Car("black", 75, 20000, 17, 25000, p2, e2);
		
		System.out.println(c1.getUsage() + " l/100km");
		System.out.println(c2.getUsage() + " l/100km");
		
		System.out.println(c1.getPrice() + " �");
		System.out.println(c2.getPrice() + " �");
		
		System.out.println(c1.getType());
		System.out.println(c2.getType());
		
		Person pe1 = new Person("Simon", "Meyer", LocalDate.of(1953, Month.MARCH, 13));
		pe1.addCar(c1);
		pe1.addCar(c2);
		
		System.out.println(pe1.getAge() + " years old");
		System.out.println(pe1.getValueOfCars() + "� is the total value");
	}

}
