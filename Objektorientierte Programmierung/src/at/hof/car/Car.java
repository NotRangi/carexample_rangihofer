package at.hof.car;

public class Car {
	private String color;
	private double maxVelocity;
	private double basePrice;
	private double baseUsage;
	private double currentMileage;
	private Producer producer;
	private Engine engine;
	
	public Car(String color, double maxVelocity, double basePrice, double baseUsage, double currentMileage, Producer producer, Engine engine) {
		super();
		this.color = color;
		this.maxVelocity = maxVelocity;
		this.basePrice = basePrice;
		this.baseUsage = baseUsage;
		this.currentMileage = currentMileage;
		this.producer = producer;
		this.engine = engine;
	}
	
	public double getPrice(){
		return this.getBasePrice() * ((100-this.getProducer().getDiscount())/100);
	}
	
	public double getUsage(){
		if(this.getCurrentMileage() < 50000 && this.getType()=="Benzin"){
			return this.getBaseUsage()*(getCurrentMileage()/100);
		}
		else{
			return this.getBaseUsage()*(getCurrentMileage()/100) * 1.098;
		}
	}
	
	public String getType(){
		return this.getEngine().getType();
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public double getMaxVelocity() {
		return maxVelocity;
	}

	public void setMaxVelocity(double maxVelocity) {
		this.maxVelocity = maxVelocity;
	}

	public double getBasePrice() {
		return basePrice;
	}

	public void setBasePrice(double basePrice) {
		this.basePrice = basePrice;
	}

	public double getBaseUsage() {
		return baseUsage;
	}

	public void setBaseUsage(double baseUsage) {
		this.baseUsage = baseUsage;
	}

	public Producer getProducer() {
		return producer;
	}

	public void setProducer(Producer producer) {
		this.producer = producer;
	}

	public Engine getEngine() {
		return engine;
	}

	public void setEngine(Engine engine) {
		this.engine = engine;
	}

	public double getCurrentMileage() {
		return currentMileage;
	}

	public void setCurrentMileage(double currentMileage) {
		this.currentMileage = currentMileage;
	}
	
	
	
}
